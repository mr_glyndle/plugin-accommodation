<?php
include(locate_template('partials/section_background.php'));
include(locate_template('partials/overlay.php'));
include(locate_template('partials/spacing.php'));
$cur_term = get_the_terms( $post->ID, 'accommodation_feature' );
$intro_text = get_sub_field('intro');
?>

	<div <?php if ($rgba_colour) { echo 'style="background-color:' . $rgba_colour . '"';};?>class="content s_over avs_<?php if ($add_vertical_space) { echo $add_vertical_space . ' '; } else { echo 'default '; };?> <?php if ($add_vertical_margin) { echo 'avm_' . $add_vertical_margin . ' '; }; if ($space_to_remove) { echo $space_to_remove; }; if ($alignment_over_background) { echo ' ' . $alignment_over_background; }; ?>">

		<?php
		if ($intro_text) {
			echo '<div class="txt_blk intro"><div class="intro">' . $intro_text . '</div></div>';
		};

		$features = array( );
		$i = 0;
		foreach ( $cur_term as $term_child ) {

			$term_id = $term_child->term_id;

			$feature_icon = get_field('af_ico', 'accommodation_feature' . '_' . $term_id);
			$feature_group = get_field('group', 'accommodation_feature' . '_' . $term_id);
			$feature_title = $term_child->name;

	    $features[$feature_group . '_' . $i] = array( $i => array( 'feature_group'=>$feature_group, 'feature_name'=>$feature_title, 'feature_icon' => $feature_icon) );
			++$i;
		}

		ksort($features);

		$prev_group='';
		echo '<ul>';
		foreach ($features as $group_features) {

				foreach ($group_features as $feature) {

					$group = $feature['feature_group'];

					echo '<li class="feat_item';
					if($feature['feature_icon']) {
						echo ' icon';
					}
					echo '">';
					if ($feature['feature_icon']) {
						echo '<img class="feat_icon" src="' .  $feature['feature_icon']['sizes']['thumbnail'] . '" alt="' . $feature['feature_icon']['alt'] . '" title="' . $feature['feature_icon']['title'] . '"/>';
					}
					echo '<span class="feat_title">' . $feature['feature_name'] . '</span></li>';

				$prev_group = $feature['feature_group'];

				if($group !== $prev_group) {
			}
		}
	}
	echo '</ul>'; ?>
	</div>

	<?php include(locate_template('partials/video_bg.php'));?>

</div>
