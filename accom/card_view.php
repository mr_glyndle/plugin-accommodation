<?php // vars
$accom_type = get_field('accom_type');
$max_occupancy = get_field('max_occupancy');
$max_price = get_field('max-price');
$min_price = get_field('min-price');
$no_beds = get_field('no_beds');
$no_rooms = get_field('no_rooms');
$pets = get_field('pets');
$map = get_field('map');?>

<div class="item ic_<?php echo $grid_item_count; ++$grid_item_count;?>">
	<div class="txt_bg"></div>
	<a class="thumbnail_link image_container" href="<?php echo get_permalink(); ?>">
		<?php if (has_post_thumbnail()) {
			the_post_thumbnail('golden_medium', array('class' => 'lazy'));
		} else {
			echo '<img class="placeholder" src="' . get_template_directory_uri() . '/img/placeholder.png" alt="' . get_the_title() . '">';
		}?>
	</a>
	<?php if ($min_price && $max_price) {
					echo '<p class="small price">£' . $min_price . ' - £' . $max_price . '</p>';
				}; ?>
	<div class="text">
		<div class="text_content">
			<h4><a href="<?php echo get_permalink(); ?>"><?php the_title(); ?></a></h4>
			<p class="small"><?php echo excerpt(55); ?></p>

			<?php $accom_tags = get_the_terms( $post->ID, 'accommodation_feature');

				echo '<p class="small tags">';
				if ($pets == "True") {
					echo 'Pet Friendly | ';
				}
				if($no_rooms) {
					echo $no_rooms . ' bedroom';
					if ($no_rooms > 1) {
						echo 's';
					}
					echo ' | ';
				}
				if ($no_beds) {
					echo $no_beds . ' bed';
					if ($no_beds > 1) {
						echo 's';
					}
					echo ' | ';
				}
				if($max_occupancy) {
					echo 'Sleeps ' . $max_occupancy;
				}
				echo '</p>';

			if ( get_post_status() == 'private' ) {
				show_publish_button();
			};?>
			<p class="small link"><a href="<?php echo get_permalink(); ?>" class="btn">More...</a></p>
		</div>
	</div>
	<?php if(!empty($accom_type)) { if($accom_type !== 'None') {?>
	<script type="application/ld+json"> {
		"@context": "http://schema.org"
		,"@type": ["<?php echo $accom_type ?>", "Product"]
		,"name": "<?php echo get_the_title(); ?>"
		,"@id": "<?php echo get_permalink() . '#accommodation'; ?>"
		,"description": "<?php echo htmlspecialchars(get_the_excerpt(), ENT_QUOTES); ?>"

		<?php if(get_permalink()) { echo ', "url": ' . json_encode(get_permalink()) . ','; }; ?>
		"brand": {
			"@type" : "http://schema.org/Brand"
			,"name": "<?php echo bloginfo( 'name' ); ?>"
		}
		<?php if ($accom_type == 'HotelRoom' || $accom_type == 'Suite') { ?>
		,"bed": {
			"@type" : "BedDetails"
			,"numberOfBeds" : "<?php echo $no_beds; ?>"
		}
		<?php } else { ?>
		,"numberOfRooms" : "<?php echo $no_rooms; ?>"
		<?php }
		if ($accom_type !== 'House') {
			echo ',"occupancy": {
				"@type" : "QuantitativeValue"
				,"maxValue" : "' . $max_occupancy . '"
				,"unitCode" : "C62"
			}';
		}
		if ($min_price !== '') {
			echo ',"offers": {
				"@type": "AggregateOffer",
				"offerCount" : "1",
				"lowPrice": "' . $min_price . '",
				"highPrice": "' . $max_price . '",
				"priceCurrency": "GBP"
			}';
		}
		if( !empty($map) ) {
			echo ',"geo": {
			    "@type": "GeoCoordinates",
			    "latitude": " ' . $map['lat'] . '",
			    "longitude": "' . $map['lng'] . '"
			}';
		}

		if ( class_exists( 'Stars_Rating' ) ) {
			$args = array(
				'post_id' => get_the_ID(),
				'status'  => 'approve',
				'parent' => 0
			);
			$comments = get_comments( $args );
			$ratings  = array();
			$count    = 0;
			foreach ( $comments as $comment ) {

				if ( ! empty( $rating ) ) {
					$ratings[] = absint( $rating );
					$count ++;
				}

			}
			if ( 0 != count( $ratings ) ) {
				$avg = round(array_sum( $ratings ) / count( $ratings ), 1);
				$avg_round = round($avg);

				echo ',
				"aggregateRating": {
					"@type": "AggregateRating",
					"ratingValue": "' . $avg . '",
					"ratingCount": "' . $count . '"
				}';
			}
		}
		if (has_post_thumbnail()) {
			echo ',"image": ' . json_encode(get_the_post_thumbnail_url($post->ID,'flex_height_small'));
		} else {
			echo ',"image": "' . get_template_directory_uri() . '/img/placeholder.png"';
		}
		if ($pets == "True") {?>
			,"petsAllowed": "<?php echo $pets; ?>"
		<?php } ?>
		<?php if (!empty($accom_tags)) {
			echo ',"amenityFeature": [';
			$count = count( $accom_tags );
			$current = 0;
			foreach ( $accom_tags as $tag ) {
				echo '{ "@type" : "LocationFeatureSpecification", "value": "True", "name": "'. $tag->name . '"}';
				++$current;
				if ($current < $count) {
					echo ', ';
				} else {
					echo ']}';
				}
			}
		} ?>
	}
	</script>
<?php } }?>
</div>
