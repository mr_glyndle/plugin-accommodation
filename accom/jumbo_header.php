<?php
$queried_object = get_queried_object();
if (isset($queried_object->taxonomy)) {
	$post_id = $queried_object->taxonomy.'_'.$queried_object->term_id;
	$background_image = get_field('category_image', $post_id);
	$dark_or_light = get_field('dark_or_light', $post_id);
?><div scoped class="cat_header <?php if (isset($dark_or_light)) { echo $dark_or_light; }; ?> parallax"><?php
if ($background_image) {
	$background_image = $background_image['sizes'];
	$bgimg_xl = $background_image['flex_height_xlarge'];
	$bgimg_l = $background_image['flex_height_large'];
	$bgimg_m = $background_image['flex_height_medium'];
	echo "<style scoped> .cat_header { background-image: url('" . $bgimg_m ."'); }
	@media screen and (min-width: 989px) { .cat_header { background-image: url('" . $bgimg_m ."'); } }
	@media screen and (min-width: 1280px) { .cat_header { background-image: url('" . $bgimg_l . "'); } }
	@media screen and (min-width: 2000px) { .cat_header { background-image: url('" . $bgimg_xl . "'); } } </style>";
} ?><div class="content txt_blk add_vertical_space_<?php if ($background_image) { echo '10'; } else { echo 'default'; }; ?> normal " style="text-align: center;"><h1><?php single_cat_title(); ?></h1><?php echo category_description(); ?></div></div><?php }; ?>
