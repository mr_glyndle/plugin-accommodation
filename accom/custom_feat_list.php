<?php
include(locate_template('partials/section_background.php'));
include(locate_template('partials/overlay.php'));
include(locate_template('partials/spacing.php'));
$intro_text = get_sub_field('intro');
?>

	<div <?php if ($rgba_colour) { echo 'style="background-color:' . $rgba_colour . '"';};?>class="content s_over avs_<?php if ($add_vertical_space) { echo $add_vertical_space . ' '; } else { echo 'default '; };?> <?php if ($add_vertical_margin) { echo 'avm_' . $add_vertical_margin . ' '; }; if ($space_to_remove) { echo $space_to_remove; }; if ($alignment_over_background) { echo ' ' . $alignment_over_background; }; ?>">

		<?php
		if ($intro_text) {
			echo '<div class="txt_blk intro"><div class="intro">' . $intro_text . '</div></div>';
		};

		// check if the repeater field has rows of data
		if( have_rows('feature_items') ):

			echo '<ul>';
		 	// loop through the rows of data
			while ( have_rows('feature_items') ) : the_row();

				$feature_icon = get_sub_field('icon');
				$feature_title = get_sub_field('feature_title');

				if ($feature_icon || $feature_title) {?>
					<li class="feat_item <?php if($feature_icon) { echo 'icon'; } ?>">
						<?php if ($feature_icon) {
							echo '<img class="feat_icon" src="' .  $feature_icon['sizes']['thumbnail'] . '" alt="' . $feature_icon['alt'] . '" title="' . $feature_icon['title'] . '"/>';
						}
						if($feature_title) {
							echo '<span class="feat_title">' . $feature_title . '</span>';
						}
					echo '</li>';
				}
		    endwhile;
			echo '</ul>';
		else :
			// no rows found
		endif; ?>
	</div>
	<?php include(locate_template('partials/video_bg.php'));?>
</div>
