<?php get_header(); ?>
<?php if (have_posts()) {

	// vars
	$accom_type = get_field('accom_type');
	$max_occupancy = get_field('max_occupancy');
	$no_beds = get_field('no_beds');
	$no_rooms = get_field('no_rooms');
	$max_price = get_field('max-price');
	$min_price = get_field('min-price');
	$pets = get_field('pets');
	$schema_title = get_the_title();
	$schema_blog_name = get_bloginfo( 'name' );
	$map = get_field('map');


	$GLOBALS['footer-schema'] .= '"@context": "http://schema.org","@type": ["' . $accom_type . '", "Product"],
	"name": "' . $schema_title . '"';

	if(get_permalink()) {
		$GLOBALS['footer-schema'] .= ',
		"url": ' . json_encode(get_permalink()) . ',
		"@id": "' . get_permalink() . '#accommodation"';
	}

	$GLOBALS['footer-schema'] .= ',
	"description": ' . json_encode(get_the_excerpt());

	$GLOBALS['footer-schema'] .= ',
	"brand": {
		"@type" : "http://schema.org/Brand",
		"name": "' . $schema_blog_name . '"
	}';

	if( $accom_type == 'HotelRoom' || $accom_type == 'Suite' ) {
		$GLOBALS['footer-schema'] .= ',
		"bed": {
			"@type" : "BedDetails",
			"numberOfBeds" : "' . $no_beds . '"
		}';
	} else {
		$GLOBALS['footer-schema'] .= ',
		"numberOfRooms" : "' . $no_rooms . '"';
	}

	if ($accom_type !== 'House') {
		 $GLOBALS['footer-schema'] .= ',
		 "occupancy": {
			 "@type" :
			 	"QuantitativeValue",
			 	"maxValue" : "' . $max_occupancy . '",
				"unitCode" : "C62"
			}';
	}
	if ($min_price !== '') {
		$GLOBALS['footer-schema'] .= ',
		"offers": {
			"@type": "AggregateOffer",
			"lowPrice": "' . $min_price . '",
			"highPrice": "' . $max_price . '",
			"priceCurrency": "GBP",
			"offerCount": "1"
		}';
	}

	$GLOBALS['footer-schema'] .= ',
	"petsAllowed": "' . $pets . '"';

	if(get_the_post_thumbnail_url()) {
		$GLOBALS['footer-schema'] .= ',
		"image": {
			"@type": "imageObject",
			"url": ' . json_encode(get_the_post_thumbnail_url(get_the_ID(),'golden_medium')) . ',
			"height": "632px",
			"width": "898px"
		}';
	}
	if( !empty($map) ) {
		$GLOBALS['footer-schema'] .= ',"geo": {
		    "@type": "GeoCoordinates",
		    "latitude": " ' . $map['lat'] . '",
		    "longitude": "' . $map['lng'] . '"
		}';
	}

	while (have_posts()) : the_post(); ?>

		<?php // check if the flexible content field has rows of data
    	if( have_rows('cont') ) {
			$item_count = 1;
			// loop through the rows of data
			while ( have_rows('cont') ) : the_row();

				// Make these reusable functions rather than repeatedly calling files?
				include(locate_template('partials/slice_loop.php'));


			endwhile;



		// Is there a map for this item?
		$location = get_field('map');
		if (isset($location) && $location !== '') {
			include( plugin_dir_path( __FILE__ ) . '/map/single-map.php');
		}

		} else { ?>
			<div class="txt_blk"><?php the_content();?></div>
	 	<?php }; ?>

	   	<?php endwhile; ?>
		<?php } else { ?>

		<div class="txt_blk">
			<div class="alert alert-info">
				<h1>Sorry, we can't find the page you're looking for</h1>
				<p>Please use the navigation or, seach the site with the options in the menu.</p>
			</div>
		</div>

		<?php };

		if (comments_open() || get_comments_number()) { ?><div class="txt_blk comments"><?php comments_template(); ?></div><?php };

get_footer();
