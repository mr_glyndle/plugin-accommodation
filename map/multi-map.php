<?php

include( plugin_dir_path( __FILE__ ) . '/map-script.php');


// Get posts from current category and tags
$args = array(
	'post_type' => 'accom',
	'orderby'	=>	'title',
	'order'	=>	'ASC',
	'posts_per_page'   => -1,
	'tax_query' => array(
		'relation' => 'AND',
		array(
			'taxonomy' => 'accommodation_type',
			'field'    => 'term_id',
			'terms'    => $term_id,
			'include_children' => true,
		),
		array(
			'taxonomy'	=> 'accommodation_feature',
			'field'		=> 'term_id',
			'terms'		=> $filter_tag_ids,
			'operator' => 'AND',
			'include_children' => true,
		),
	),
);
$query = new WP_Query( $args );


	echo '<div class="gmap">';

	while ( $query->have_posts() ) : $query->the_post();

		$map = get_field('map');

		if( !empty($map) ) {

			echo '<div class="marker" data-lat="'. $map['lat'] . '" data-lng="' . $map['lng'] . '">';
			echo '<h5><a href="' . get_permalink() . '">' .  get_the_title() . '</a></h5>';
			echo '<p class="address">' . get_the_excerpt() . '</p>';
			echo '</div>';

		}

	endwhile;

	echo '</div>';
